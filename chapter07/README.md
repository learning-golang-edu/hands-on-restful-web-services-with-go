# Working with PostgreSQL, JSON, and Go

## Docker setup

Examples in this chapter are using Postgres database.
`docker-compose.yml` files contains configuration of docker container for Postgres database. To start container execute
following command:

```shell
docker-compose up -d
```

This will start docker container with Postgres database and will create user and database used by the examples in this
chapter.

## Implementing a URL-sortening service using PostgreSQL and pq

Execute following command to add URL:

```shell
curl -X POST http://localhost:8000/v1/short -H 'cache-control: no-cache' -H 'content-type: application/json' -d '{"url": "https://www.packtpub.com/eu/game-development/unreal-engine-4-shaders-and-effects-cookbook"}'
```

This will return the shortened URL string:

```json
{
  "encoded_string": "1"
}
```

To get real URL execute following command:

```shell
curl -X GET http://localhost:8000/v1/short/1
```

This will return actual URL:

```json
{
  "url": "https://www.packtpub.com/eu/game-development/unreal-engine-4-shaders-and-effects-cookbook"
}
```

## Exploring the JSONStore feature in PostgreSQL

To create a new package execute following command:

```shell
curl -X POST 
  http://localhost:8000/v1/package 
  -H 'cache-control: no-cache' 
  -H 'content-type: application/json' 
  -d '{
      "dimensions": {
       "width": 21,
        "height": 12
      },
      "weight": 10,
      "is_damaged": false,
      "status": "In transit"
     }'
```

This will insert new package into the database and return the newly created entry's ID:

```json
{
  "id": 1
}
```

To get this package information execute following command:

```shell
curl -X GET http://localhost:8000/v1/package/1 
```

This will be returning selected package JSON:
```json
{
  "Package": {
    "ID": 1,
    "CreatedAt": "2021-01-14T14:10:59.711014Z",
    "UpdatedAt": "2021-01-14T14:10:59.711014Z",
    "DeletedAt": null,
    "Data": "{\"status\": \"In transit\", \"weight\": 10, \"dimensions\": {\"width\": 21, \"height\": 12}, \"is_damaged\": false}"
  }
}
```

To query for packages with particular weight execute following command:
```shell
curl -X GET 'http://localhost:8000/v1/package?weight=10'
```

It should return following JSON:
```json
{
  "Package": {
    "ID": 1,
    "CreatedAt": "2021-01-14T14:10:59.711014Z",
    "UpdatedAt": "2021-01-14T14:10:59.711014Z",
    "DeletedAt": null,
    "Data": "{\"status\": \"In transit\", \"weight\": 10, \"dimensions\": {\"width\": 21, \"height\": 12}, \"is_damaged\": false}"
  }
}
```
