package main

import (
	"fmt"
	"github.com/emicklei/go-restful"
	"io"
	"log"
	"net/http"
	"time"
)

func pingTime(_ *restful.Request, resp *restful.Response) {
	// Write to the response
	io.WriteString(resp, fmt.Sprintf("%s", time.Now()))
}

func main() {
	// Create a web service
	webservice := new(restful.WebService)
	// Create a route and attach it to handler in the service
	webservice.Route(webservice.GET("/ping").To(pingTime))
	// Add the service to application
	restful.Add(webservice)
	log.Fatal(http.ListenAndServe(":8000", nil))
}
