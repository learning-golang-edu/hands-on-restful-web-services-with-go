# Simplifying RESTful Services with Popular Go Frameworks

## Building Metro API with go-restful

Test with the following flow.

First create a new train:
```shell
curl -X POST http://localhost:8000/v1/trains -H 'cache-control: no-cache' -H 'content-type: application/json' -d '{"driverName": "Veronica", "operatingStatus": true}'
```

Get the train information by id:
```shell
curl http://localhost:8000/v1/trains/1
```

This will return selected train JSON.

Then delete this train:
```shell
curl -X DELETE "http://localhost:8000/v1/trains/1"
```

When now try to get this train:
```shell
curl -v http://localhost:8000/v1/trains/1
```

It will return `Train could not be found` error message.

## Building RESTful API with the Gin framework

Test it with the following flow.

First create a new station:
```shell
curl -X POST http://localhost:8000/v1/stations -H 'cache-control: no-cache' -H 'content-type: application/json' -d '{"name": "Brooklyn", "opening_time": "8:12:00", "closing_time": "18:23:00"}'
```

Get the station information by id:
```shell
curl http://localhost:8000/v1/stations/1
```

This will return selected station JSON.

Then delete this station:
```shell
curl -X DELETE "http://localhost:8000/v1/stations/1"
```

When now try to get this station:
```shell
curl -v http://localhost:8000/v1/stations/1
```

It will return `Station could not be found` error message.

## Building RESTful API with revel.go

To run app execute following command in the project root (`chapter4`):
```shell
revel run src/railAPIRevel
```

The application will start with single train already inserted.
Test flow is following.

Get first train:
```shell
curl -X GET "http://localhost:8000/v1/trains/1"
```

This will return following result:
```json
{
 "id": 1,
 "driver_name": "Logan",
 "operating_status": true
}
```

Create a new train:
```shell
curl -X POST http://localhost:8000/v1/trains -H 'cache-control: no-cache' -H 'content-type: application/json' -d '{"driver_name":"Magneto", "operating_status": true}'
```

This will insert a new train with new id `2`.
To get it execute following command:
```shell
curl -X GET "http://localhost:8000/v1/trains/2"
```

This will return following result:
```json
{
  "id": 2,
  "driver_name": "Logan",
  "operating_status": true
}
```
