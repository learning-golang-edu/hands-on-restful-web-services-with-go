# Working with MongoDB and Go to create a REST API

## MongoDB setup

There is `docker-compose.yml` file in the root of this project.
To run mongoDB just start docker container with the following command:
```shell
docker-compose up -d
```

This will download required docker image and start mongoDB inside docker container.

**NOTE** docker engine must be running and `docker-compose` command must be installed and be accessible in the path.

## RESTful API with gorilla/mux and MongoDB

To insert the movie execute following command:
```shell
curl -X POST http://localhost:8000/v1/movies -H 'cache-control: no-cache' -H 'content-type: application/json' -d '{ "name" : "The Dark Knight", "year" : "2008", "directors" : [ "Christopher Nolan" ], "writers" : [ "Jonathan Nolan", "Christopher Nolan" ], "boxOffice" : { "budget" : 185000000, "gross" : 533316061 }}'
```

This will insert the movie into database and return inserted movie ID:
```json
{"InsertedID":"5ffedde4f4eeda21b6f48830"}
```

This ID can be used to query the movie information from the database:
```shell
curl -X GET http://localhost:8000/v1/movies/5ffedde4f4eeda21b6f48830
```

The movie can be deleted with the following command:
```shell
curl -X DELETE -v http://localhost:8000/v1/movies/5ffedde4f4eeda21b6f48830
```

When trying to retrieve deleted movie:
```shell
curl -X GET -v http://localhost:8000/v1/movies/5ffedde4f4eeda21b6f48830
```

Status code `404 Not Found` and error message `Movie cannot be found` will be returned.
