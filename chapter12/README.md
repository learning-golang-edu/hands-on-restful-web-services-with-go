# Chapter 12: Containerizing REST Services for Deployment

## Installing the Nginx server

Directory `nging-intro` have a `docker-compose.yml` file, which includes minimal configurations to run simple `nginx`
server. To start the container execute following command:

```shell
docker-compose up -d
```

This will start `nginx` server. It can accessed from the following URL [http://localhost](http://localhost).

## Deploying a Go service using Nginx

Directory `bookServer` contains sample HTTP server. To start this server execute following command in this directory:

```shell
go run main.go
```

To test this server execute following command:

```shell
curl -X GET "http://localhost:8000/api/books"
```

This will return following JSON response:

```json
{
  "ID": 123,
  "ISBN": "0-201-03801-3",
  "Author": "Donald Knuth",
  "PublishedYear": "1968"
}
```

## Makefile and Docker Compose-based deployment

Directory `deploySetup` contains Go server with docker and nginx configurations needed to run all components as docker
containers. To build project and start up the project execute following command in this directory:

```shell
make
```

This will build Go server and start docker containers.

This is equivalent to the following command:

```shell
docker-compose up --build -d
```

Now server can be accessed at the followig URL [http://localhost/api/books](http://localhost/api/books).

It will return following JSON:

```json
{
  "ID": 123,
  "ISBN": "0-201-03801-3",
  "Author": "Donald Knuth",
  "PublishedYear": "1968"
}
```
