package main

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"hash/fnv"
	"log"
	"math"
	"net/http"
	"time"
)

const (
	base         = 62
	characterSet = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"
)

func toBase62(num uint64) string {
	b := make([]byte, 0)

	// loop as long the num is bigger than zero
	for num > 0 {
		// receive the rest
		r := math.Mod(float64(num), float64(base))

		// devide by Base
		num /= base

		// append chars
		b = append([]byte{characterSet[int(r)]}, b...)
	}

	return string(b)
}

func Encode(str string) string {
	h := fnv.New64a()
	h.Write([]byte(str))
	w := h.Sum64()
	return toBase62(w)
}

var urls = make(map[string]string)

type urlStruct struct {
	Url string
}

func EncodeUrl(w http.ResponseWriter, r *http.Request) {
	var t urlStruct
	err := json.NewDecoder(r.Body).Decode(&t)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, "failed parse request: %v", err)
		return
	}
	url := t.Url
	encoded := Encode(url)
	if encoded == "" {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	urls[encoded] = url
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "Shortened URL: %v\n", encoded)
}

func DecodeUrl(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	shortUrl := vars["url"]
	originalUrl := urls[shortUrl]
	if originalUrl == "" {
		w.WriteHeader(http.StatusNotFound)
		return
	}
	http.Redirect(w, r, originalUrl, http.StatusMovedPermanently)
}

func main() {
	r := mux.NewRouter()
	r.HandleFunc("/api/v1/new", EncodeUrl).Methods("POST")
	r.HandleFunc("/api/v1/{url}", DecodeUrl)
	srv := &http.Server{
		Handler:      r,
		Addr:         "127.0.0.1:8000",
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}
	log.Fatal(srv.ListenAndServe())
}
