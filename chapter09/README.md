# Asynchronous API design

`docker-compose.yml` file have configuration for running RabbitHQ and Redis.
Start container by executing following command:
```shell
docker-compose up -d
```

To check running containers execute following command:
```shell
docker ps
```

It will output something like this:
```shell
CONTAINER ID   IMAGE        COMMAND                  CREATED         STATUS         PORTS                                                                                              NAMES
3db97383623e   rabbitmq:3   "docker-entrypoint.s…"   2 minutes ago   Up 2 minutes   4369/tcp, 0.0.0.0:5672->5672/tcp, 5671/tcp, 15691-15692/tcp, 25672/tcp, 0.0.0.0:15672->15672/tcp   rabbitmq-server-go
bb76fe5eedcf   redis        "docker-entrypoint.s…"   2 minutes ago   Up 2 minutes   0.0.0.0:6379->6379/tcp                                                                             redis-go
```

## Delaying API jobs with queuing

This sample consists of sender and receiver applications.

First start receiver application with the following command:
```shell
go run src/basicReceiver/main.go
```

This will start receiver application and produce following output:
```shell
2021/01/15 17:59:37 Worker has started
```

Then execute sender application:
```shell
go run src/basicSender/main.go
```

This will send a message to the queue and produce following output:
```shell
2021/01/15 18:00:37 Successfully published a message to the queue
```

At the other end receiver will consume this message and output following:
```shell
2021/01/15 18:00:37 Received a message from the queue: 2021-01-15 18:00:37.690395 +0100 CET m=+0.018519900
```

Can stop receiver application by pressing `Ctrl+C`.

## Long-running task design

To start this example execute following command:
```shell
go run src/longRunningTaskV1/main.go
```
Output:
```shell
2021/01/18 10:46:37 Workers are booted up and running
```

To test execute following commands:
```shell
curl -X GET "http://localhost:8000/job/database?client_time=1569177495"
```
Output:
```shell
{"uuid":"81987091-5f32-44cd-831d-2bd6209dd38c","type":"A","extra_data":{"client_time":"2019-09-22T20:38:15+02:00"}}
```

```shell
curl -X GET http://localhost:8000/job/callback
```
Output:
```shell
{"uuid":"9b866dd1-cf51-45e4-bf60-cb707cd9c8cb","type":"B","extra_data":""}
```

```shell
curl -X GET http://localhost:8000/job/mail
```
Output:
```shell
{"uuid":"339f8302-e194-4671-bdea-8bddb5c6cea6","type":"C","extra_data":""}
```

Instead of blocking the requests, the server returns quickly with a Job ID for the tasks. Let's take a look at the server logs:
```shell
2021/01/18 10:49:26 Workers received a message from the queue:{81987091-5f32-44cd-831d-2bd6209dd38c A map[client_time:2019-09-22T20:38:15+02:00]}
2021/01/18 10:49:26 Worker A: extracting data..., JOB: map[client_time:2019-09-22T20:38:15+02:00]
2021/01/18 10:49:28 Worker A: saving data to database..., JOB: 81987091-5f32-44cd-831d-2bd6209dd38c
2021/01/18 10:50:10 Workers received a message from the queue:{9b866dd1-cf51-45e4-bf60-cb707cd9c8cb B }
2021/01/18 10:50:10 Worker B: performing some long running process..., JOB: 9b866dd1-cf51-45e4-bf60-cb707cd9c8cb
2021/01/18 10:50:20 Worker B: posting the data back to the givencallback..., JOB: 9b866dd1-cf51-45e4-bf60-cb707cd9c8cb
2021/01/18 10:50:44 Workers received a message from the queue:{339f8302-e194-4671-bdea-8bddb5c6cea6 C }
2021/01/18 10:50:44 Worker C: sending the email..., JOB: 339f8302-e194-4671-bdea-8bddb5c6cea6
2021/01/18 10:50:46 Worker C: sent the email successfully, JOB: 339f8302-e194-4671-bdea-8bddb5c6cea6
```

## Caching strategies for APIs

### Redis example

To see redis example in action execute following command:
```shell
go run src/redisIntro/main.go
```

It will produce following output:
```shell
PONG
```

### Long-running task with Redis

To start this application execute following command:
```shell
go run src/longRunningTaskV2/main.go
```

This will start server and produce following output:
```shell
2021/01/19 10:04:16 Workers are booted up and running
```

To start a job execute following command:
```shell
curl -X GET "http://localhost:8000/job/database?client_time=1569177495"
```

This will respond with the JSON like the following:
```shell
{"uuid":"7be8851e-fe45-40af-b96a-110d1ba2ca53","type":"A","extra_data":{"client_time":"2019-09-22T20:38:15+02:00"}}
```

To get the status of a job with uuid execute following command:
```shell
curl -X GET "http://localhost:8000/job/status?uuid=7be8851e-fe45-40af-b96a-110d1ba2ca53"
```

This returns the following status:
```shell
{"status":"DONE","uuid":"7be8851e-fe45-40af-b96a-110d1ba2ca53"}
```
