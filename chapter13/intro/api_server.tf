provider "aws" {
  profile = "default"
  region = "eu-central-1"
}

resource "aws_key_pair" "api_server_key" {
  key_name   = "api-server-key"
  public_key = "RSA_PUBLIC_KEY"
}

resource "aws_instance" "api_server" {
  ami = "ami-0093cac2bf998a669"
  instance_type = "t2.micro"
  key_name = aws_key_pair.api_server_key.key_name
}
