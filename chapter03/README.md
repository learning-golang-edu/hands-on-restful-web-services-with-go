# Chapter 3: Working with Middleware and RPC

To test JSON RPC server execute following command:
```shell
curl -X POST http://localhost:1234/rpc -H 'cache-control: no-cache' -H 'content-type: application/json' -d '{"method": "JSONServer.GiveBookDetail", "params": [{"ID": "1234"}], "id": "1"}'
```

**NOTE:** JSON RPC server should be running
