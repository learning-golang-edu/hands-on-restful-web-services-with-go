# Handling Authentication for our REST Services

## How simple authentication works

Directory `simpleAuth` have simple application with authentication.
To run this simple application execute following command:
```shell
go run  main.go
```

This will start application which can be tested for example with the following URL
[http://localhost:8000/healthcheck](http://localhost:8000/healthcheck).

This should return `Forbidden (403)` response.

## Persisting client sessions with Redis

In directory `simplaAuthWithRedis` is sample application with connection to Redis store.
Before starting the application docker container must be started to make Redis available:
```shell
docker-compose up -d
```

Now application can be started with the following command:
```shell
go run main.go
```

You can use Postman to login and do the health check of the application. 
After application restart the health check will work, because authentication information is stored in the Redis store and is retrieved from there.

Stored key can be checked from the store. First start Redis command tool:
```shell
docker exec -it some-redis redis-cli
```

In the shell execute followig command:
```shell
KEYS *
```
```shell
1) "session_42KJ6KA24WRFMP2XTJYLWNSOSJFKBEAQPIPGGJW227ZJT5ZHFVZA"
```
This is the session key stored in Redis.
