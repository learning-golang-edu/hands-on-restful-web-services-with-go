package main

import (
	"context"
	"fmt"

	proto "encryptClient/proto"

	"github.com/micro/micro/v3/service"
)

func main() {
	srv := service.New()
	client := proto.NewEncryptService("encryptService", srv.Client())

	// Call the encrypter
	rsp, err := client.Encrypt(context.TODO(), &proto.Request{
		Message: "I am a Message",
		Key:     "111023043350789514532147",
	})

	if err != nil {
		fmt.Println(err)
	}

	// Print response
	fmt.Println(rsp.Result)

	// Call the decrypter
	rsp, err = client.Decrypt(context.TODO(), &proto.Request{
		Message: rsp.Result,
		Key:     "111023043350789514532147",
	})

	if err != nil {
		fmt.Println(err)
	}

	// Print response
	fmt.Println(rsp.Result)
}
