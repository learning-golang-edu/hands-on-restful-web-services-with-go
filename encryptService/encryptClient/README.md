# EncryptClient Service
**_Building an RPC client with Go Micro_**

This is the EncryptClient service

## Usage

Generate the proto code
```
make proto
```

Run the client
```
micro run .
```

When successful it will not print any output. 

But it is possible to check the status of micro services:
```shell
micro status
```
```shell
NAME            VERSION SOURCE                                                                STATUS  BUILD   UPDATED         METADATA
encryptClient   latest  ~/hands-on-restful-web-services-with-go/encryptService/encryptClient  running n/a     12s ago         owner=admin, group=micro
encryptService  latest  ~/hands-on-restful-web-services-with-go/encryptService/encryptService running n/a     1h7m38s ago     owner=admin, group=micro
helloworld      latest  github.com/micro/services/helloworld                                  running n/a     1h23m58s ago    owner=admin, group=micro
```

Now, since example service client is also running, it should be possible to see it’s logs:
```shell
micro logs encryptClient
```
```shell
8/+JCfT7+ibIjzQtmCo=
I am a Message
8/+JCfT7+ibIjzQtmCo=
I am a Message
```
