package handler

import (
	"context"
	proto "encryptService/proto"
)

type EncryptService struct{}

// Encrypt converts a message into cipher and returns response
func (g *EncryptService) Encrypt(_ context.Context,
	req *proto.Request, rsp *proto.Response) error {
	rsp.Result = EncryptString(req.Key, req.Message)
	return nil
}

// Decrypt converts a cipher into message and returns response
func (g *EncryptService) Decrypt(_ context.Context,
	req *proto.Request, rsp *proto.Response) error {
	rsp.Result = DecryptString(req.Key, req.Message)
	return nil
}
