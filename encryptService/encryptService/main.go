package main

import (
	"encryptService/handler"
	pb "encryptService/proto"

	"github.com/micro/micro/v3/service"
	"github.com/micro/micro/v3/service/logger"
)

func main() {
	// Create service
	srv := service.New(
		service.Name("encryptService"),
		service.Version("latest"),
	)

	// Register handler
	pb.RegisterEncryptServiceHandler(srv.Server(), new(handler.EncryptService))

	// Run service
	if err := srv.Run(); err != nil {
		logger.Fatal(err)
	}
}
