# EncryptService Service
**_Building a microservice with Go Micro_**

This is the EncryptService service

Generated with

```
micro new encryptService
```

## Usage

Generate the proto code

```
make proto
```

Run the service

```
micro run .
```

## Go Micro basic commands

**NOTE** micro server must be running. 

To start Go Micro server execute following command:
```shell
micro server
```

This will start the go micro server in an interactive mode. 

Before interacting with the `micro server`, we need to log in with the id ‘admin’ and password ‘micro’:
```shell
micro login
```
```shell
Enter email address: admin
Enter Password:
Successfully logged in.
```

If all goes well you’ll see log output from the server listing the services as it starts them. Just to verify that everything is in order, let’s see what services are running:
```shell
micro services
```
```shell
api
auth
broker
config
encryptservice
events
network
proxy
registry
runtime
server
store
```

`encryptservice` will be seen in the list.

To check the status of the running service, execute following command:
```shell
micro status
```
```shell
NAME		VERSION	SOURCE									STATUS	BUILD	UPDATED		METADATA
encryptService	latest	~/hands-on-restful-web-services-with-go/encryptService/encryptService	running	n/a	13m53s ago	owner=admin, group=micro
helloworld	latest	github.com/micro/services/helloworld					running	n/a	30m13s ago	owner=admin, group=micro
```
