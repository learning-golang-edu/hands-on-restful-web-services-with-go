# Building a REST API Client in Go

## Basics for writing a command-line tool in Go

### Examples with flag

Go have build-in package `flag` to handle command line arguments. Below are examples of this package usage.

When execute this command without any arguments:

```shell
go run src/basic/flagExample.go
```

It will use default value for argument `name` and output following:

```shell
2021/01/14 15:26:58 Hello stranger, Welcome to the command line world
```

To see usage message should pass `-h` option:

```shell
go run src/basic/flagExample.go -h
```

This will output following:

```shell
Usage of <...>/flagExample:
  -name string
        your wonderful name (default "stranger")
```

Argument can be passed as following:

```shell
go run src/basic/flagExample.go -name "John Doe"
```

or

```shell
go run src/basic/flagExample.go -name="John Doe"
```

This will output following:

```shell
2021/01/14 15:32:38 Hello John Doe, Welcome to the command line world
```

Here's example of multiple arguments command:

```shell
go run src/basic/flagExampleMultiParam.go -name "John Doe" -age 33
```

Which will output following:

```shell
2021/01/14 15:35:33 Hello John Doe (33 years), Welcome to the command line world
```

### cli package examples

`cli` package provides more complex arguments handling.

The usage output is much more informative:

```shell
go run src/cli/sampleOne/main.go -h
```

Here's the output:

```shell
NAME:
   main - A new cli application

USAGE:
   main [global options] command [command options] [arguments...]

VERSION:
   0.0.0

COMMANDS:
   help, h  Shows a list of commands or help for one command

GLOBAL OPTIONS:
   --name value   your wonderful name (default: "stranger")
   --age value    your graceful age (default: 0)
   --help, -h     show help
   --version, -v  print the version
```

Run following to see it in use:

```shell
go run src/cli/sampleOne/main.go -name "John Doe"
```

It will output following:

```shell
2021/01/14 15:56:05 Hello John Doe (0 years), Welcome to the command line world
```

And here's second example of using `cli` package with combination of flags and arguments. Here's the program usage:

```shell
go run src/cli/sampleTwo/main.go -h
```

```shell
NAME:
   main - A new cli application

USAGE:
   main [global options] command [command options] [arguments...]

VERSION:
   1.0

COMMANDS:
   help, h  Shows a list of commands or help for one command

GLOBAL OPTIONS:
   --save value   Should save to database (yes/no) (default: "no")
   --help, -h     show help
   --version, -v  print the version
```

To see it in action execute following command:

```shell
go run src/cli/sampleTwo/main.go --save=yes "John Doe" 89 85 97
```

This will produce the following output:

```shell
2021/01/14 16:04:18 Person:  John Doe
2021/01/14 16:04:18 marks [89 85 97]
2021/01/14 16:04:18 Saving to the database [John Doe 89 85 97]
```

When `save` option is set to `no`:

```shell
go run src/cli/sampleTwo/main.go --save=no "John Doe" 89 85 97
```

It will produce the following output:

```shell
2021/01/14 16:06:27 Person:  John Doe
2021/01/14 16:06:27 marks [89 85 97]
2021/01/14 16:06:27 Skipping saving to the database
```

## Cobra, an advanced CLI library

`cobra` is another Go package to handle command line arguments. Here's usage output build with this package:

```shell
go run src/cobraExample/main.go -h
```

```shell
A long string about description

Usage:
  details [flags]

Flags:
  -a, --age int       Age of the student (default 25)
  -h, --help          help for details
  -n, --name string   Name of the student (default "stranger")
```

Here's sample usage:

```shell
go run src/cobraExample/main.go details -n "John Doe" -a 33
```

Which outputs following:

```shell
2021/01/14 17:07:50 Hello John Doe (33 years), Welcome to the command line world
```

## Getting comfortable with the GitHub REST API

To start first need to get `GITHUB_TOKEN`. Generate one for your GitHub account and set it as your environment variable:

```shell
export GITHUB_TOKEN=YOUR_GITHUB_ACCESS_TOKEN
```

To run sample application execute following command:

```shell
go run src/githubAPI/main.go
```

It will return something like the following:

```shell
2021/01/15 16:15:27 [{79171906 libdc-for-dirk torvalds/libdc-for-dirk 33 false} {2325298 linux torvalds/linux 35278 false} {113099837 pesconvert torvalds/pesconvert 32 false} {78665021 subsurface-for-dirk torvalds/subsurface-for-dirk 41 false} {86106493 test-tlb torvalds/test-tlb 119 false} {117900805 uemacs torvalds/uemacs 111 false}]
```

## Create a CLI tool as an API client for the GitHub REST API

`GITHUB_TOKEN` environment variable is needed for this tool, so setup it as in previous example.

When run command without any arguments:
```shell
go run src/gitTool/main.go
```

This will print tool usage message:
```shell
NAME:
   main - A new cli application

USAGE:
   main [global options] command [command options] [arguments...]

VERSION:
   1.0

COMMANDS:
   fetch, f   Fetch the repo details with user. [Usage]: githubAPI fetch user
   create, c  Creates a gist from the given text. [Usage]: githubAPI name 'description' sample.txt
   help, h    Shows a list of commands or help for one command

GLOBAL OPTIONS:
   --help, -h     show help
   --version, -v  print the version
```

To fetch repository data execute following command:
```shell
go run src/gitTool/main.go f torvalds
```

This returns all the repositories that belong to the great Linus Torvalds.
```shell
2021/01/15 16:40:24 [{79171906 libdc-for-dirk torvalds/libdc-for-dirk 33 false} {2325298 linux torvalds/linux 35278 false} {113099837 pesconvert torvalds/pesconvert 32 false} {78665021 subsurface-for-dirk torvalds/subsurface-for-dirk 41 false} {86106493 test-tlb torvalds/test-tlb 119 false} {117900805 uemacs torvalds/uemacs 111 false}]
```

Gist can be create with the following command:
```shell
go run src/gitTool/main.go c "I am doing well" sample1.txt sample2.txt
```

This will create Gist in GitHub account.
