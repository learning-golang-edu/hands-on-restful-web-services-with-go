# GraphQL and Go

To run examples in this chapter `GITHUB_TOKEN` is required. 
Generate one for your GitHub account and place it into `.env` file with the following content:
```shell
# .env file
GITHUB_TOKEN=YOUR_GITHUB_ACCESS_TOKEN
```

## Creating GraphQL clients in Go

To run the program execute following command:
```shell
go run src/graphqlClient/main.go
```

This prints the license description to the console:
```shell
2021/01/19 11:36:24 A permissive license whose main conditions require preservation of copyright and license notices. Contributors provide an express grant of patent rights. Licensed works, modifications, and larger works may be distributed under different terms and without source code.
```

## Creating GraphQL servers in Go

To run this program execute following command:
```shell
go run src/graphqlServer/main.go
```

This starts a GraphQL server on `localhost:8000/graphql`.
Open a browser and visit http://localhost:8000/graphql. 
Interactive GraphQL editor will be shown. 
Now, paste the following client query into the left-hand pane:
```
query {
  players {
    highScore
    id
    isOnline
    levelsUnlocked
  }
}
```

The response served by GraphQL server will be shown in the right-hand pane:
```json
{
  "data": {
    "players": [
      {
        "highScore": "1100",
        "id": 123,
        "isOnline": true,
        "levelsUnlocked": []
      },
      {
        "highScore": "2100",
        "id": 230,
        "isOnline": false,
        "levelsUnlocked": []
      }
    ]
  }
}
```
