package main

import (
	"context"
	"github.com/joho/godotenv"
	"github.com/machinebox/graphql"
	"log"
	"os"
)

// Response of APi
type Response struct {
	License struct {
		Name        string `json:"name"`
		Description string `json:"description"`
	} `json:"license"`
}

func main() {
	// create a client (safe to share across requests)
	client := graphql.NewClient("https://api.github.com/graphql")

	// make a request to GitHub API
	req := graphql.NewRequest(`
		query {
			license(key: "apache-2.0") {
				name
				description
			}
		}
`)

	err := godotenv.Load(".env")
	if err != nil {
		log.Fatalln("Error loading .env file")
	}
	githubToken := os.Getenv("GITHUB_TOKEN")
	req.Header.Add("Authorization", "bearer "+githubToken)

	// define a Context for the request
	ctx := context.Background()

	// run it and capture the response
	var respData Response
	if err := client.Run(ctx, req, &respData); err != nil {
		log.Fatal(err)
	}
	log.Println(respData.License.Description)
}
