# AsyncService Service

This is the AsyncService service

Generated with

```
micro new asyncService
```

## Usage

Generate the proto code

```
make proto
```

Run the service

```
micro run .
```