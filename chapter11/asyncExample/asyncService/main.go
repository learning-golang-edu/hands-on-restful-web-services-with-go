package main

import (
	pb "asyncService/proto"
	"context"
	"log"
	"time"

	"github.com/micro/micro/v3/service"
	"github.com/micro/micro/v3/service/logger"
)

func main() {
	// Create service
	srv := service.New(
		service.Name("weather"),
		service.Version("latest"),
	)

	p := service.NewEvent("alerts")

	go func() {
		for now := range time.Tick(15 * time.Second) {
			log.Println("Publishing weather alert to Topic: alerts")
			err := p.Publish(context.TODO(), &pb.Event{
				City:        "Munich",
				Timestamp:   now.UTC().Unix(),
				Temperature: 2,
			})
			if err != nil {
				logger.Warn("failed to publish to the topic", err)
			}
		}
	}()
	srv.Init()

	// Run service
	if err := srv.Run(); err != nil {
		logger.Fatal(err)
	}
}
