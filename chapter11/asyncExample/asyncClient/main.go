package main

import (
	pb "asyncService/proto"
	"context"

	"github.com/micro/micro/v3/service"
	"github.com/micro/micro/v3/service/logger"
)

// ProcessEvent processes a weather alert
func ProcessEvent(_ context.Context, event *pb.Event) error {
	logger.Info("Got alert:", event)
	return nil
}

func main() {
	srv := service.New(service.Name("weather_client"))
	srv.Init()

	err := service.Subscribe("alerts", ProcessEvent)
	if err != nil {
		logger.Warn("failed to subscribe to topic", err)
	}

	if err := srv.Run(); err != nil {
		logger.Fatal(err)
	}
}
