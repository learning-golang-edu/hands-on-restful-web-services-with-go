# Scaling our REST API Using Microservices

## Understanding encryption

To run this example execute following command:
```shell
go run src/encryptString/main.go
```
```shell
2021/01/19 13:43:27 Original message:  I am A Message
2021/01/19 13:43:27 Encrypted message:  8/+JCfTb+ibIjzQtmCo=
2021/01/19 13:43:27 Decrypted message:  I am A Message
```

## Building a microservices with Go Micro
[EcnryptService with Go Micro](../encryptService/encryptService/README.md)

## Building an RPC client with Go Micro
[EcnryptClient with Go Micro](../encryptService/encryptClient/README.md)
